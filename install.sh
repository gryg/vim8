#!/bin/bash
NVIM_CONF_DIR=~/.config/nvim
VIM_CONF_DIR=~/.vim
VIMRC=~/.vimrc
NVIMRC=$NVIM_CONF_DIR/init.vim

echo "Downloading vim-plug ... (${VIM_CONF_DIR})"
curl -fLo ${VIM_CONF_DIR}/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "Symlinking vimrc to ~/.vimrc"
ln -sf ${VIM_CONF_DIR}/vimrc ${VIMRC}
