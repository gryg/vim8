call plug#begin('~/.vim/plugged')

" Core plugin
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mhinz/vim-startify'
Plug 'maxbrunsfeld/vim-yankstack'
Plug 'tpope/vim-repeat'
Plug 'vim-scripts/zoomwintab.vim'
Plug 'sjl/gundo.vim'
Plug 'neomake/neomake'
Plug 'shougo/neocomplete.vim'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
"" Coding
Plug 'majutsushi/tagbar'
Plug 'Raimondi/delimitMate'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'ntpeters/vim-better-whitespace'
"" Browsing, directory, files
Plug 'vim-scripts/The-NERD-tree', {'on': 'NERDTreeToggle'}
Plug 'vim-scripts/bufexplorer.zip'
Plug 'vim-scripts/LargeFile'
Plug 'travisjeffery/vim-auto-mkdir'
Plug 'vim-scripts/DirDiff.vim'
"" Git
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
"" Search
Plug 'terryma/vim-multiple-cursors'
Plug 'kien/ctrlp.vim'
Plug 'rking/ag.vim'
"" Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'ervandew/supertab'
"" Themes
Plug 'w0ng/vim-hybrid'
Plug 'altercation/vim-colors-solarized'
Plug 'morhetz/gruvbox'
Plug 'jnurmine/Zenburn'
Plug 'dracula/vim'

" Language specific
"" Rust
Plug 'rust-lang/rust.vim'
"" Python
Plug 'jmcantrell/vim-virtualenv'
Plug 'fs111/pydoc.vim'
Plug 'hynek/vim-python-pep8-indent'

" Syntax
"" Markdown
Plug 'plasticboy/vim-markdown'
"" CSV
Plug 'chrisbra/csv.vim'
"" CSS
Plug 'wavded/vim-stylus'
Plug 'vim-scripts/Better-CSS-Syntax-for-Vim'
"" HTML5
Plug 'othree/html5.vim'
Plug 'Rykka/riv.vim'
"" Ansible and SaltStack
Plug 'saltstack/salt-vim'
Plug 'pearofducks/ansible-vim'
"" Miscs ...
Plug 'pearance/vim-tmux'
Plug 'vim-scripts/crontab.vim'
Plug 'Shougo/vinarise.vim'
Plug 'martinda/Jenkinsfile-vim-syntax'

call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" GENERAL
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if has("vms")
    set nobackup	" do not keep a backup file, use versions instead
else
    set backup		" keep a backup file
endif

set showcmd		" display incomplete commands
set scrolloff=2
set wildmode=longest,list
set viminfo='1000,f1,:500,/500,<50,s10,h
set listchars+=tab:I.,trail:_
set nowrap
set wildignore=*.o,*.obj,*.bak,*.exe,*~
set lz			" do not redraw while running macros (much faster) (LazyRedraw)
set hid			" you can change buffer without saving
set autoread		" update file without asking

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" APPEARANCE
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if &t_Co > 2 || has("gui_running")
    "" Comment or Uncomment this line according to your terminal configuration
    "" (256 colors or not)
    set t_Co=256
    syntax on
    set hlsearch
endif
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set number
set cursorline
"popup max height
set pumheight=15

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Font & Color
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set guifont=Monospace\ 10
set background=dark
"let g:solarized_termtrans=1
"let g:solarized_termcolors=256
"let g:solarized_contrast="high"
"let g:solarized_visibility="high"
"colorscheme solarized

"colorscheme dracula
colorscheme gruvbox
"colorscheme zenburn
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ENCODING & FILETYPE
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set fileencoding=utf8
set encoding=utf8
syntax on
filetype on            " enables filetype detection
filetype plugin on     " enables filetype specific plugins
filetype plugin indent on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" INDENTATION
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set expandtab
set softtabstop=4
set tabstop=4
set shiftwidth=4
set smarttab
set autoindent

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" FORMATING
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"set textwidth=80
highlight col79 ctermbg=Red guibg=Red

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SEARCHING
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set ignorecase
set smartcase
set nowrapscan
set incsearch		" do incremental searching

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" OTHER
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
source ~/.vim/filetype.vim
source ~/.vim/langdep.vim
source ~/.vim/plugin.vim
source ~/.vim/mappings.vim
